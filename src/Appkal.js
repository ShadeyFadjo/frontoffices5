import logo from './logo.svg';
import './App.css';
import Fiche from './components/Fiche/fiche';
import Login from './components/Fiche/login';
import Footer from './components/Fiche/footer';
import Header from './components/Fiche/header';
import React, { useState, useEffect } from 'react';
function App() {

  const enchere={
    utilisateur:'Jean',
    categorie:'Automobile',
    produit:'Express',
    descri:'Mila vola maika, 50000 km, mi-rill tsara',
    prixplanche:25000000,
    duree:72,
    dateajout:'2023-01-13',
    highest:40000000
  }

  const [posts, setPosts] = useState(null);
   useEffect(() => {
      fetch('https://enchere-production-cd97.up.railway.app/get_byid/1')
         .then((response) => response.json())
         .then((data) => {
            console.log(data);
            setPosts(data);
         })
         .catch((err) => {
            console.log(err);
         });
   }, []);

   //const valiny=JSON.parse(JSON.stringify(posts));
  //  posts.forEach((post)=>{
    
  //  })
   
   
    
    return ( /* 
      <div className="App">
        <header className="App-header">
          
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Mandehh <code>src/App.js</code> and save to reload.
            Crack le benoit?
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>*/
      
      <div className="container">
         <div className="item-a"><Header/></div>
         <div className="item-b">
            <Fiche 
              highest={enchere.highest} 
              duree={enchere.duree} 
              prixplanche={enchere.prixplanche} 
              categorie={enchere.categorie} 
              produit={enchere.produit} 
              descri={enchere.descri} 
              utilisateur={enchere.utilisateur} 
              dateajout={enchere.dateajout}/>
          </div>
         <div className="item-c"><Login/></div>
        <hr/>
        <div className="item-d"><Footer/></div>
      </div>
    );
}
export default App;