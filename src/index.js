import React from 'react';
import ReactDOM from 'react-dom/client';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
  
import './index.css';
import './App.css';

import App from './App';
import reportWebVitals from './reportWebVitals';
import FilterableProductTable from './components/Login/Liste.js';
import FilterableTableProduit from './components/Liste/ListEnchere.js';
import { ExampleComponent } from './components/Liste/ListEnchere.js';
import Login from './components/Fiche/login';
import Footer from './components/Fiche/footer';
import Header from './components/Fiche/header';
// import {IconTabs} from './components/Tab.js'
const PRODUCTS = [
  {category: 'Telephone', price: '$49.99', stocked: true, name: 'Iphone X',dateajout:'22/2/2023'},
  {category: 'Accessoires', price: '$9.99', stocked: true, name: 'Ecouteurs',dateajout:'6/11/2002'},
  {category: 'Sporting Goods', price: '$29.99', stocked: false, name: 'Basketball',dateajout:'7/8/2023'},
  {category: 'Electronics', price: '$99.99', stocked: true, name: 'iPod Touch',dateajout:'8/11/2006'},
  {category: 'Electronics', price: '$399.99', stocked: false, name: 'iPhone 5',dateajout:'15/12/2003'},
  {category: 'Electronics', price: '$199.99', stocked: true, name: 'Nexus 7',dateajout:'15/12/2003'}
];
const P=[{"idutilisateur":1,"categorie":"Telephone","produit":"Iphone 8","duree":4,"description":"bonne occasion mora be","idenchere":1,"prix_planche":2400000,"ajout":"2023-01-24T05:43:39.013+00:00","etat":1,"nom":"Rak","prenom":"Ben","email":"rak@yahoo.mg"},{"idutilisateur":1,"categorie":"Appareils menagers","produit":"Four","duree":7,"description":"tsara be","idenchere":2,"prix_planche":645000,"ajout":"2023-01-24T05:43:39.047+00:00","etat":0,"nom":"Rak","prenom":"Ben","email":"rak@yahoo.mg"},{"idutilisateur":1,"categorie":"Telephone","produit":"Iphone 12","duree":4,"description":"bonne occasion","idenchere":3,"prix_planche":3500000,"ajout":"2023-01-24T05:43:39.054+00:00","etat":0,"nom":"Rak","prenom":"Ben","email":"rak@yahoo.mg"},{"idutilisateur":1,"categorie":"Voitures","produit":"golf type 3","duree":4,"description":"mbola d origine","idenchere":4,"prix_planche":8000000,"ajout":"2023-01-24T05:43:39.063+00:00","etat":0,"nom":"Rak","prenom":"Ben","email":"rak@yahoo.mg"},{"idutilisateur":1,"categorie":"Voitures","produit":"V8","duree":12,"description":"","idenchere":5,"prix_planche":48000000,"ajout":"2023-01-24T05:43:39.071+00:00","etat":0,"nom":"Rak","prenom":"Ben","email":"rak@yahoo.mg"},{"idutilisateur":1,"categorie":"Voitures","produit":"406","duree":13,"description":"","idenchere":6,"prix_planche":240000,"ajout":"2023-01-24T05:43:39.080+00:00","etat":1,"nom":"Rak","prenom":"Ben","email":"rak@yahoo.mg"},{"idutilisateur":1,"categorie":"Maison","produit":"Villa Tsarahonenana","duree":7,"description":"villa en securite","idenchere":7,"prix_planche":500000,"ajout":"2023-01-24T05:43:40.825+00:00","etat":0,"nom":"Rak","prenom":"Ben","email":"rak@yahoo.mg"}];
const root = ReactDOM.createRoot(document.getElementById('root'));




// const test=<ExampleComponent/>;
root.render(
  <React.StrictMode>
   
    <div className="container">
         <div className="item-a"><Header/></div>
         {/* {test} */}
       
  {/* <div className='item-b'>    <FilterableTableProduit products={P} /></div> */}
  <div className='item-b'>    <ExampleComponent /></div>
   {/* < App/> */}
         <div className="item-d"><Footer/></div></div>
     
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
