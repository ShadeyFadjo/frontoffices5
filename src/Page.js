import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
  } from "react-router-dom";
import App from './App';
    
function Page()
{
    <Router>
    <Switch>
      {/* This route is for home component 
      with exact path "/", in component props 
      we passes the imported component*/}
      <Route exact path="/" component={App} />
        
      {/* This route is for about component 
      with exact path "/about", in component 
      props we passes the imported component*/}
      <Route path="/about" component={App} />
        
      {/* This route is for contactus component
      with exact path "/contactus", in 
      component props we passes the imported component*/}
      <Route path="/contactus" component={App} />
        
      {/* If any route mismatches the upper 
      route endpoints then, redirect triggers 
      and redirects app to home component with to="/" */}
      <Redirect to="/1" />
    </Switch>
  </Router>
}
export default Page;