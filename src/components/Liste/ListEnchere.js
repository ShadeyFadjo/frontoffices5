import React,{Component} from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import  { useState, useEffect } from 'react';
class EnchereCategoryRow extends React.Component { //chaque categorie enchere
    render() {
      const category = this.props.category;
      return (
        <tr>
          <th colSpan="3">
            {category}
          </th>
        </tr>
      );
    }
  }


  class LigneProduit extends React.Component { //chaque ligne produit
    render() {
      const product = this.props.product;
      const name = product.stocked ?
        product.name :
        <span style={{color: 'red'}}>
          {product.produit}
        </span>;
  
      return (
        <tr>
          <td> <a href="./homes"> {product.produit}</a>  </td>
          <td>{product.description}</td>
          <td>{product.ajout}</td>
        </tr>

      );
    }
  }
  
  export class TableProduit extends React.Component { //La table produit
    render() {
      const filterText = this.props.filterText;
      const inStockOnly = this.props.inStockOnly;
  
      const rows = [];
      let lastCategory = null;
      this.props.products.forEach((product) => {
        // if (product.name.indexOf(filterText) === -1&&product.price.indexOf(filterText) === -1) {
        if (product.produit.indexOf(filterText) === -1&&product.description.indexOf(filterText) === -1) {
          return;
        }
        if (inStockOnly && !product.etat==1) {
          return;
        }
        if (product.categorie !== lastCategory) {
          rows.push(
            <EnchereCategoryRow
              category={product.categorie}
              key={product.categorie} />
          );
        }
        rows.push(
          <LigneProduit
            product={product}
            key={product.idenchere}
          />
        );
        lastCategory = product.categorie;
      });
  
      return (
        <table border={1}>
          <thead>
            <tr>
              <th>Produit</th>
              <th>Description</th>
              <th>Date d'ajout</th>
            </tr>
          </thead>
          <tbody>{rows}</tbody>
        </table>
      );
    }
  }
  
  class SearchBar extends React.Component { //La barre de recherche
    constructor(props) {
      super(props);
      this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
      this.handleInStockChange = this.handleInStockChange.bind(this);
    }
    
    handleFilterTextChange(e) {
      this.props.onFilterTextChange(e.target.value);
    }
    
    handleInStockChange(e) {
      this.props.onInStockChange(e.target.checked);
    }
    
    render() {
      return (
        <form>
          <input
            type="text"
            placeholder="Search..."
            value={this.props.filterText}
            onChange={this.handleFilterTextChange}
          />
          <p>
            <input
              type="checkbox"
              checked={this.props.inStockOnly}
              onChange={this.handleInStockChange}
            />
            {' '}
           Seulement les encheres valables
          </p>
        </form>
      );
    }
  }
  
 export class FilterableTableProduit extends React.Component { //Table complete
    constructor(props) {
      super(props);
      this.state = {
        filterText: '',
        inStockOnly: false
      };
      
      this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
      this.handleInStockChange = this.handleInStockChange.bind(this);
    }
  
    handleFilterTextChange(filterText) {
      this.setState({
        filterText: filterText
      });
    }
    
    handleInStockChange(inStockOnly) {
      this.setState({
        inStockOnly: inStockOnly
      })
    }
    
    render() {
      return (
        <div>
          <SearchBar
            filterText={this.state.filterText}
            inStockOnly={this.state.inStockOnly}
            onFilterTextChange={this.handleFilterTextChange}
            onInStockChange={this.handleInStockChange}
          />
            {/* <ExampleComponent   
            filterText={this.state.filterText}
            inStockOnly={this.state.inStockOnly}/> */}

          <TableProduit
            products={this.props.products}
            filterText={this.state.filterText}
            inStockOnly={this.state.inStockOnly}
          />


        </div>
      );
    }
  }



export const ExampleComponent = (props) => {
  const [data, setData] = useState(null);
  
   useEffect(() => {
     fetch('http://localhost:8081/getAllEnchere/')
      .then(response => response.json())
      .then(data => setData(data)) 
      .catch(error => console.error(error));
  }, [] );

  return (
    <div>
      {data === null ? (
        <p>Loading...</p>
      ) : (
        <ul>
        {/* //   {data.map(item => ( */}
        {/* //     // <li key={item.produit}>{item.produit}</li> */}
        {/* //   ))} */}
          {/* <TableProduit
            // products={props.products}
            products={JSON.stringify(data)}
            filterText={props.filterText}
            inStockOnly={props.inStockOnly}
          /> */}
                  <FilterableTableProduit products={data} />
                  {/* export JSON.stringify(data); */}

        </ul>
      )}
    </div>
  );
};

// export default ExampleComponent;

  export default FilterableTableProduit;