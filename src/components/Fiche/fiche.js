import React, {Component} from 'react';
import Login from './login';

class Fiche extends Component {
  render() {
    return (
        
        <div className="Fiche">
           
            <h1> {this.props.produit}</h1>
            <h5>Catégorie: {this.props.categorie}</h5>
            <h5> {this.props.descri}</h5>
            
            <h4>Mis en enchère par {this.props.utilisateur} ,le {this.props.dateajout}</h4>
            <h5>Durée de l'enchère : {this.props.duree} heures</h5>
            <h5>Prix planché = {this.props.prixplanche} Ar</h5>
            <h2>Prix à battre actuellement = {this.props.highest} Ar</h2>
        </div>
    );
  }
}
export default Fiche;